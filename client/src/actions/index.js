export const updateUserName = (myname) => {
    return {
        type: "UPDATEUSERNAME",
        userName: myname
    }
}