import "bootstrap/dist/css/bootstrap.min.css";
import AuthUser from './components/AuthUser';
import Guest from './navbar/guest';
import Auth from './navbar/auth';
import './styles.css';
import store from "./store";
import {Provider} from "react-redux"
//store.subscribe(() => console.log(store.getState()));

function App() {
  const {getToken} = AuthUser();
  if(!getToken()){
    return <Provider store={store}><Guest /></Provider>
  }
  return (
    <Provider store={store}><Auth /></Provider>
  );
}

export default App;
