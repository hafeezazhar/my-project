import { Container, Row, Col, Button } from 'react-bootstrap';
export default function Home() {
    return(
        <div><Container fluid>
        <Row className="bg-light p-4">
          <Col>
            <h1 className="display-3">Welcome to my website</h1>
            <p className="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.
            </p>
            <Button variant="primary">Learn more</Button>
          </Col>
          <Col>
            <img src="https://picsum.photos/500/500" alt="Placeholder" className="img-fluid" />
          </Col>
        </Row>
        <Row className="my-5">
          <Col md={4}>
            <h2>Feature 1</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.</p>
          </Col>
          <Col md={4}>
            <h2>Feature 2</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.</p>
          </Col>
          <Col md={4}>
            <h2>Feature 3</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.</p>
          </Col>
        </Row>
        <Row className="bg-light p-4">
          <Col>
            <h2>Contact Us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mauris eget ex blandit malesuada.</p>
            <Button variant="primary">Get in touch</Button>
          </Col>
          <Col>
            <img src="https://picsum.photos/500/500" alt="Placeholder" className="img-fluid" />
          </Col>
        </Row>
      </Container></div>
    )
}