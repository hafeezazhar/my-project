import React from 'react';

function ErrorComponent(props) {
  return (
    <div className="alert alert-danger alert-dismissible fade show">
        <strong>Error!</strong> {props.errorMessage}                        
    </div>
  );
}

export default ErrorComponent;
