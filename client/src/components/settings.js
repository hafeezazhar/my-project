import { useState,useEffect } from "react"
import { useNavigate } from 'react-router-dom';
import AuthUser from './AuthUser';
import ErrorComponent from './error';
import { useAlert } from 'react-alert';
import { useDispatch } from 'react-redux';
import { updateUserName } from '../actions/index';




export default function Settings() {
    const navigate = useNavigate();
    const {http} = AuthUser();
    const [sources,setSources] = useState();
    const [name,setName] = useState();
    const [authors,setAuthors] = useState();
    const [errorMessage, setErrorMessage] = useState(null);
    const alert = useAlert();
    const dispatch = useDispatch(); 

    const submitForm = () =>{
        // api call
        http.post('/save-settings',{sources:sources,authors:authors,name:name}).then((res)=>{
            alert.success("Preferences updated");
            dispatch(updateUserName(name));
            navigate('/dashboard')
        })
        .catch((error) => {
            //setErrorMessage(error.response.data.message);
            const erObject = error.response.data.errors;
            if (typeof erObject != "undefined") {
                for (const key in erObject) {
                    alert.error(erObject[key]);               
                }
            } else {
                alert.error(error.response.data.message);      
            }     
          });
    }

    //load fresh contents
    useEffect(() => {
        http.get('/me').then((res)=>{
            console.log("hhh="+JSON.stringify(res.data));
            setSources(res.data.prefered_sources.split(','));
            setAuthors(res.data.prefered_authors.split(','));
            setName(res.data.name);
        })
        .catch(error => {
            console.error(error);
        }); 
    }, []); 

    
    return(
        <div className="row justify-content-center pt-5">
            <div className="col-sm-6">
                <div className="card p-4">
                    <h1 className="text-center mb-3">Preferences </h1>
                    {errorMessage && <ErrorComponent errorMessage={errorMessage} />}

                    <div className="form-group mt-3">
                        <label>Name:</label>
                        <input type="text" className="form-control" value={name} placeholder="Enter name"
                            onChange={e=>setName(e.target.value)}
                        id="name" />
                    </div>
                    
                    <div className="form-group mt-3">
                    <label htmlFor="sources" className="form-label">
                        Sources
                        </label>
                        <select
                        multiple
                        className="form-control"
                        id="sources"
                        value={sources}
                        onChange={(event) => setSources(Array.from(event.target.selectedOptions, (option) => option.value))}
                        >
                        <option value="">All</option>
                        <option value="News API">News API</option>
                        <option value="New York Times">New York Times</option>                        
                        </select>
                    </div>

                    <div className="form-group mt-3">
                    <label htmlFor="authors" className="form-label">
                        Authors
                        </label>
                        <select
                        multiple
                        className="form-control"
                        id="authors"
                        value={authors}
                        onChange={(event) => setAuthors(Array.from(event.target.selectedOptions, (option) => option.value))}
                        >
                        <option value="">All</option>
                        <option value="Laravel News">Laravel News</option>
                        <option value="Stack Overflow">Stack Overflow</option>
                       
                        </select>
                    </div>
                    <button type="button" onClick={submitForm} className="btn btn-primary mt-4">Save</button>
                </div>
            </div>
        </div>
    )
}