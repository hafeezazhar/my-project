import { Form } from 'react-bootstrap';

function SearchFilter({ onSearchChange, onFilterChange, searchQuery, categoryFilter }) {
  return (
    <Form className="mb-4">
      <Form.Group className="mb-2">
        <Form.Control
          type="text"
          placeholder="Search articles"
          onChange={onSearchChange}
          value={searchQuery}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          as="select"
          onChange={onFilterChange}
          value={categoryFilter}
        >
          <option value="">All Sources</option>
          <option value="News API">News API</option>
          <option value="New York Times">New York Times</option>
        </Form.Control>
      </Form.Group>
    </Form>
  );
}

export default SearchFilter;
