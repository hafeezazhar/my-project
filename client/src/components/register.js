import { useState } from "react"
import { useNavigate } from 'react-router-dom';
import AuthUser from './AuthUser';
import { useAlert } from 'react-alert'

export default function Register() {
    const navigate = useNavigate();
    const {http} = AuthUser();
    const [name,setName] = useState();
    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const alert = useAlert();
   
    const submitForm = () =>{
        // api call
        http.post('/register',{email:email,password:password,name:name}).then((res)=>{
            alert.success("Please login to continue");
            navigate('/login')
        })
        .catch((error) => {
            const erObject = error.response.data.errors;
            if (typeof erObject != "undefined") {
                for (const key in erObject) {
                    alert.error(erObject[key]);               
                }
            } else {
                alert.error(error.response.data.message);      
            }     
          });
    }

    return(
        <div className="row justify-content-center pt-5">
            <div className="col-sm-6">
                <div className="card p-4">
                    <h1 className="text-center mb-3">Register </h1>
                    <div className="form-group">
                        <label>Name:</label>
                        <input type="text" className="form-control" placeholder="Enter name"
                            onChange={e=>setName(e.target.value)}
                        id="name" />
                    </div>
                    <div className="form-group mt-3">
                        <label>Email address:</label>
                        <input type="email" className="form-control" placeholder="Enter email"
                            onChange={e=>setEmail(e.target.value)}
                        id="email" />
                    </div>

                    <div className="form-group mt-3">
                        <label>Password:</label>
                        <input type="password" className="form-control" placeholder="Enter password"
                            onChange={e => setPassword(e.target.value)}
                        id="pwd" />
                    </div>
                    <button type="button" onClick={submitForm} className="btn btn-primary mt-4">Register</button>
                </div>
            </div>
        </div>
    )
}