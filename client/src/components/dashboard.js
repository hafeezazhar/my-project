import { useEffect, useState } from 'react';
import AuthUser from './AuthUser';
import { Card, Button, Form } from 'react-bootstrap';
import SearchFilter from './search';

function LoadMoreButton({ handleLoadMore }) {
  return (
    <div className="d-flex justify-content-center mt-4">
      <Button variant="primary" onClick={handleLoadMore} className="w-100">
        Load More
      </Button>
    </div>
  );
}

export default function Dashboard() {
  const {http} = AuthUser();
  const [articles, setArticles] = useState([]);
  const [displayedArticles, setDisplayedArticles] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [categoryFilter, setCategoryFilter] = useState("");
  const [numArticlesDisplayed, setNumArticlesDisplayed] = useState(6);

  useEffect(()=>{
      fetchArticles();
  }, []);

  // Handle search input change
  const handleSearchChange = (event) => {
      const query = event.target.value.toLowerCase();
      const filteredArticles = articles.filter((article) => {
      return (
          article.title.toLowerCase().includes(query) 
          //|| article.description.toLowerCase().includes(query)
      );
      });
      setDisplayedArticles(filteredArticles);
      setSearchQuery(query);
      setNumArticlesDisplayed(6);
  };

  // Handle category filter change
  const handleFilterChange = (event) => {
      const category = event.target.value;
      setCategoryFilter(category);
      setNumArticlesDisplayed(6);
      if (category === "") {
      setDisplayedArticles(articles);
      } else {
      const filteredArticles = articles.filter(
          (article) => article.source === category
      );
      setDisplayedArticles(filteredArticles);
      }
  };

  const fetchArticles = () =>{
      http.get('/articles').then((res)=>{
          setArticles(res.data.data);
          setDisplayedArticles(res.data.data.slice(0, numArticlesDisplayed));

      })
      .catch(error => {
          console.error(error);
        });   
  }

  const handleLoadMore = () => {
      setNumArticlesDisplayed(numArticlesDisplayed + 6);
      setDisplayedArticles(articles.slice(0, numArticlesDisplayed + 6));
  }

  return (
      <>
          <h1>Articles</h1>
          <div className="row">
            <SearchFilter
              onSearchChange={handleSearchChange}
              onFilterChange={handleFilterChange}
              searchQuery={searchQuery}
              categoryFilter={categoryFilter}
            ></SearchFilter> 
            {/* Display the current page of articles */}
            {displayedArticles.length?displayedArticles.map(article => (
              <div key={article.id} className="col-sm-6 col-md-4 mb-4">
                  <Card style={{ width: '100%' }}>
                  <Card.Img variant="top" src={article.url_to_image} style={{ height: '10rem', objectFit: 'cover' }} />
                  <Card.Body>
                      <Card.Title>{article.title.substring(0, 30)}...</Card.Title>
                      <Card.Text>{article.description? (article.description.substring(0, 100)):'No Description'}...</Card.Text>
                      <Button variant="primary" target="blank" href={article.url}>Read more</Button>
                  </Card.Body>
                  </Card>
              </div>
            )):(<div className="alert alert-warning w-100" role="alert">Articles not found</div>)}
          </div>

          {/* Display load more button */}
          {numArticlesDisplayed < articles.length && (
            <LoadMoreButton handleLoadMore={handleLoadMore} />
          )}
      </>
  );
}
