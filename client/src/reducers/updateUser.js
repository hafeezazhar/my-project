const initialState = 'HAFEEZ222';
const updateUserName = (state = initialState,action) => {
    switch(action.type){
        case "UPDATEUSERNAME" : return action.userName;
        default : return initialState;
    }
}

export default updateUserName;