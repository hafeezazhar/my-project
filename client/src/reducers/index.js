import updateUserName from './updateUser';
import {combineReducers} from "redux";
const rootReducers = combineReducers({
    updateUserName
});

export default rootReducers;