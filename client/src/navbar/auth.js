import { Routes, Route, Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import Home from '../components/home';
import Dashboard from '../components/dashboard';
import Settings from '../components/settings';
import AuthUser from '../components/AuthUser';
import {useSelector} from 'react-redux';


function Auth() {
  const { token, logout } = AuthUser();
  const myName = useSelector((state) => state.updateUserName );

  const logoutUser = () => {
    if (token != undefined) {
      logout();
    }
  }

  return (
    <>
      <Navbar bg="primary" variant="dark" expand="sm">
        <Navbar.Brand href="/">My App</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
              <Nav.Link as={Link} to="/" className="ml-sm-4">Home</Nav.Link>
              <Nav.Link as={Link} to="/dashboard" className="ml-sm-4">Articles</Nav.Link>
              <Nav.Link as={Link} to="/settings" className="ml-sm-4">Preferences</Nav.Link>
              </Nav>
              <Nav>
              <Nav.Link role="button" onClick={logoutUser}>Logout {myName}</Nav.Link>
              </Nav>
          </Navbar.Collapse>
      </Navbar>

      <div className="container">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/settings" element={<Settings />} />
        </Routes>
      </div>
    </>
  );
}

export default Auth;
