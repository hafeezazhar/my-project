<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
//Route::get('articles', 'ArticleController@index');
Route::group([ 'middleware' => ['api', 'auth:api'] ], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@user');
    Route::get('articles', 'ArticleController@index');
    Route::post('save-settings', 'AuthController@saveSettings');
    Route::get('me', 'AuthController@me');
});

