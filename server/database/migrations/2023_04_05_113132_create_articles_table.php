<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id(); // Primary key column for the table
            $table->string('title'); // Title of the article
            $table->text('description')->nullable(); // Description of the article
            $table->text('url')->nullable(); // URL of the article, must be unique
            $table->text('url_to_image')->nullable(); // URL of the article's main image
            $table->timestamp('published_at')->nullable(); // Publication date of the article
            $table->string('category')->nullable(); // Category of the article
            $table->string('author')->nullable()->index(); // Author of the article
            $table->string('source',50)->nullable()->index(); // Source of the article
            $table->timestamps(); // Timestamps for the creation and last update of the article
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles'); // Drop the articles table
    }
}
