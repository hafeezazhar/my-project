#!/bin/sh
composer install --no-dev --no-interaction --no-progress --optimize-autoloader
chown -R www-data:www-data /var/www/html/storage
php artisan migrate
php artisan schedule:run
php artisan l5-swagger:generate
php artisan serve --host=0.0.0.0 --port=8000

