# Laravel Backend For News Aggregator App

## Available Scripts

In the project directory, you can run:

### `composer install`
### `php artisan serve`

Runs the app in the development mode.\
Open [http://localhost:8000](http://localhost:8000) to view it in your browser.
