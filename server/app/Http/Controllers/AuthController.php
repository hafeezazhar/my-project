<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User as UserResource;
use App\Models\Article;
use Illuminate\Support\Facades\Artisan;


class AuthController extends Controller
{
     /**
     * Register a new user.
     *
     * @OA\Post(
     *     path="/api/register",
     *     summary="Register a new user",
     *     tags={"Authentication"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="User's name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="User's email address",
     *                     type="string",
     *                     format="email"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="User's password",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Registration successful",
     *         @OA\JsonContent(
     *             @OA\Property(property="access_token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"),
     *             @OA\Property(property="token_type", type="string", example="bearer"),
     *             @OA\Property(property="expires_in", type="integer", example="3600"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=false),
     *             @OA\Property(property="error", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     */
    public function register(UserRegisterRequest $request)
    {
        $request->merge([
            'password' => bcrypt($request->password)
        ]);

        $user = User::create($request->all());

        if (!$token = auth()->login($user)) {
            return response()->json([
                'status'=>false,
                'error' => 'Unauthorized'               
            ], 401);        
            
        }

        return $this->respondWithTokenAndData($token, new UserResource($user));
    }

    /**
     * @OA\POST(
     *     path="/api/login",
     *     tags={"Authentication"},
     *     summary="Login",
     *     description="Login",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="email", type="string", example="abc2@abc.com"),
     *              @OA\Property(property="password", type="string", example="111111")
     *          ),
     *      ),
     *      @OA\Response(response=200, description="Login" ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function login(UserLoginRequest $request)
    {
        if (!$token = auth()->attempt($request->only(['email', 'password']))) {
            return response()->json([
                'status'=>false,
                'message' => 'Sorry user does not exists!'               
            ], 422);            
        } 
        return $this->respondWithTokenAndData($token, new UserResource($request->user()));
    }

    /**
     * Logout a user.
     *
     * Logs out the currently authenticated user.
     *
     * @OA\Post(
     *      path="/api/logout",
     *      tags={"Authentication"},
     *      summary="Logout a user",
     *      operationId="logout",
     *      security={{"jwt":{}}},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="status", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Successfully logged out")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="status", type="boolean", example=false),
     *              @OA\Property(property="error", type="string", example="Unauthorized")
     *          )
     *      )
     * )
     */
    public function logout()
    {
        auth()->logout();
       
        return response()->json([
            'status'=>true,
            'message' => 'Successfully logged out'            
        ], 200); 
    }

    /*Get details of logged in user*/

    public function user(Request $request)
    {
        return new UserResource($request->user());
    }

    /*Refresh token*/
    public function refresh()
    {
        return $this->respondWithTokenAndData(auth()->refresh());
    }    

    protected function respondWithTokenAndData($token, $data)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);       
        
    }

     
    /**
     * @OA\GET(
     *     path="/api/me",
     *     tags={"Authentication"},
     *     summary="Authenticated User Profile",
     *     description="User Profile",
     *     @OA\Response(response=200, description="Authenticated User Profile" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     *     security={{"jwt": {} }}
     * )
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Save user settings
     *
     * @OA\Post(
     *     path="/api/settings",
     *     summary="Save user settings",
     *     description="Save user preferences such as preferred sources and authors",
     *     operationId="saveSettings",
     *     tags={"Settings"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass user preferences",
     *         @OA\JsonContent(
     *             @OA\Property(property="sources", type="array", @OA\Items()),
     *             @OA\Property(property="authors", type="array", @OA\Items()),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Preferences updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=true),
     *             @OA\Property(property="message", type="string", example="Preferences updated successfully"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=false),
     *             @OA\Property(property="error", type="string", example="Unauthenticated"),
     *         ),
     *     ),
     * )
     */
    public function saveSettings(Request $request)
    {     
        $validated = $request->validate([
            'name' => 'required|max:255',
        ]);
        $data = [];
        $data['name'] = $request->name;
        $data['prefered_sources'] = '';
        $data['prefered_authors'] = '';
        if(isset($request->sources)) {
            $data['prefered_sources'] = implode(",",$request->sources);
        }

        if(isset($request->authors)) {
            $data['prefered_authors'] = implode(",",$request->authors);
        }

        User::where('id',auth()->user()->id)->update($data);

        return response()->json([
            'status'=>true,
            'message' => 'Preferences updated successfuly'            
        ], 200);        
    }
}
