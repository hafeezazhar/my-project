<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
   
     
   
    /**
     * Retrieve articles based on user preferences.
     *
     * @OA\Get(
     *     path="/api/articles",
     *     summary="Get articles",
     *     description="Retrieve articles based on user preferences.",
     *     tags={"Articles"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=true),
     *             @OA\Property(property="message", type="string", example="Articles"),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="id", type="integer", example=1),
     *                     @OA\Property(property="title", type="string", example="Article Title"),
     *                     @OA\Property(property="description", type="string", example="Article content"),
     *                     @OA\Property(property="author", type="string", example="John Doe"),
     *                     @OA\Property(property="source", type="string", example="New York Times"),
     *                     @OA\Property(property="published_at", type="string", format="date-time", example="2023-04-09 14:00:00"),
     *                     @OA\Property(property="updated_at", type="string", format="date-time", example="2023-04-09 14:30:00"),
     *                 ),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Error message"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Error message"),
     *         ),
     *     ),
     *     security={{"jwt": {}}}
     * )
     */
    public function index()
    {
        //Apply filters according to preferences of users
        try{
            $articles = Article::when(auth()->user()->prefered_authors!='', function($query){
                return $query->whereIn('author', explode(",", auth()->user()->prefered_authors));
            })
            ->when(auth()->user()->prefered_sources!='', function($query){
                return $query->whereIn('source', explode(",", auth()->user()->prefered_sources));
            })
            ->get();
        } catch (\Exception $ex) {
            //return error
            return response()->json([
                'status'=>false,
                'message' => $ex->getMessage(),                
            ], 401);
        }
         //return success
        return response()->json([
            'status'=>true,
            'message' => 'Aricles',
            'data' => $articles,
        ], 200);
    }
}
