<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\NewyorkTimesService;

class FetchNyTimesNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:nytimesnews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch articles from New York Times';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new NewyorkTimesService(); // Instantiate the NewsApiService class
        $service->fetchNews(); // Call the getEverything method to fetch and save news articles
        $this->info('News fetched successfully!'); // Output success message
    }
}
