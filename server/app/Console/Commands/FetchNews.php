<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\NewsApiService;

class FetchNews extends Command
{
    protected $signature = 'fetch:news'; // Command signature

    protected $description = 'Fetch articles from NewsAPI'; // Command description

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $service = new NewsApiService(); // Instantiate the NewsApiService class
        $service->getEverything(); // Call the getEverything method to fetch and save news articles
        $this->info('News fetched successfully!'); // Output success message
    }
}
