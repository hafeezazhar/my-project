<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\Article;

class NewyorkTimesService
{
    public function fetchNews()
    {
        $client = new Client();
        $response = $client->request('GET', 'https://api.nytimes.com/svc/search/v2/articlesearch.json', [
            'query' => [
                'api-key' => env('NYTIMES_API_KEY'),
                'q' => 'technology', // Search term, replace with your own
                'sort' => 'newest', // Sort by newest articles first
            ],
        ]);

        if ($response->getStatusCode() == 200) {
            $articlesData = json_decode($response->getBody(), true)['response']['docs'];

            $articles = [];
            foreach ($articlesData as $articleData) {
                // Check if the article already exists in the database by title
                $existingArticle = Article::where('title', $articleData['headline']['main'])->first();
                //if (!$existingArticle && $articleData['multimedia'][0]['url']!='') {
                if (!$existingArticle ) {
                    $article = new Article();
                    $article->source = 'New York Times';
                    //$article->author = 'Azhar';
                    $article->title = $articleData['headline']['main'];
                    $article->url = $articleData['web_url'];
                    $article->published_at = date('Y-m-d H:i:s', strtotime($articleData['pub_date']));
                    $article->url_to_image = $articleData['multimedia'][0]['url'] ?('https://www.nytimes.com/'.$articleData['multimedia'][0]['url']): null;
                    $article->author = $articleData['byline']['original'] ?? null;
                    $article->save();                   
                }
            }            
        } else {
            //throw new Exception('Unable to retrieve articles from NYT Article API.');
        }
    }
}
