<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\Article;

class NewsApiService
{
    public function getEverything()
    {
        $client = new Client();
        $response = $client->get('https://newsapi.org/v2/everything', [
            'query' => [
                'apiKey' => env('NEWS_API_KEY'),
                'q' => 'laravel', // search for articles with "laravel" in the title or description
                'sortBy' => 'publishedAt', // sort articles by publication date
                'language' => 'en', // retrieve articles in English
                'pageSize' => 100, // retrieve up to 100 articles per request
                'page' => 1 // start with the first page of results
            ]
        ]);

        $articles = json_decode($response->getBody(), true)['articles'];

        foreach ($articles as $article) {
            // Check if the article already exists in the database by title
            $existingArticle = Article::where('title', $article['title'])->first();

            if (!$existingArticle && $article['urlToImage']!='') {
                // Create a new article object and save it to the database
                $newArticle = new Article;
                $newArticle->source = 'News API';
                $newArticle->author = $article['author'];
                $newArticle->title = $article['title'];
                $newArticle->description = $article['description'];
                $newArticle->url = $article['url'];
                $newArticle->url_to_image = $article['urlToImage'];
                $newArticle->published_at = Carbon::parse($article['publishedAt']);
                $newArticle->category = ''; // Save the category in the same articles table
                $newArticle->save();
            }
        }
    }
}
