# Installation Guide

Welcome to the installation guide for the "Hafeez App". This guide will help you get the application up and running on your local machine. 

## Accessing the code

You can access the code for the "Hafeez App" from the GitLab repository. Follow the link to access the repository:

- GIT Repo: https://gitlab.com/hafeezazhar/my-project

Download or clone the repository and extract its contents into the "hafeez-app" folder on your local machine.

## Checking available ports

Before running the application, ensure that the following ports are available and free on your local machine:

- Backend Server: 8000
- Frontend: 3000
- MySQL: 3306
- PhpMyAdmin: 8443

## Releasing ports

If any of the required ports are in use, release them using the following commands:

- To get the PID for port 8000: `sudo lsof -ti tcp:8000`
- To stop the process: `sudo kill -9 PID`
- To release MySQL port 3306: `sudo systemctl stop mysql`

Note: These commands are specific to Ubuntu. If you are using a different operating system, refer to the appropriate documentation.

## Installing Docker and Docker-compose

Ensure that Docker and Docker-compose are installed on your local machine. The following versions were used during development:

- Docker: 20.10.17
- Docker Compose: 1.29.2

## Building docker images and containers

Move into the extracted folder "hafeez-app" and run the following command to build docker images and containers:

- `sudo docker-compose up -d --build`

Use the following command to check running containers:

- `sudo docker ps -a`

## CRON settings (OPTIONAL)

Cron jobs are set up during container build in the "/server/start.sh" file. However, if you want to set up cron jobs yourself, follow the given instructions:

- Bash into "hafeez-app_backend_1" container
- Execute: `crontab -e`
- Provide following commands (need to provide proper paths for php and artisan):

```
* * * * * '/path/to/php' '/path/to/artisan' fetch:news > '/dev/null' 2>&1
* * * * * '/usr/bin/php' '/path/to/artisan' fetch:nytimesnews > '/dev/null' 2>&1
```

- Save and close the crontab file

## Accessing the application

In your browser, you can access the following links provided by the docker container:

- Website Frontend: http://localhost:3000/login
- PhpMyAdmin: http://localhost:8443 (Login/Password= root/root)
- Swagger API Documentation: http://localhost:8000/api/documentation

## Completed Tasks

Here is a list of the completed tasks for the "Hafeez App":

- Used Laravel for Backend and ReactJS for Frontend
- Design is responsive (Bootstrap)
- User can register
- User can login
- User can set preferences
- Article listing for logged in user
- Articles can be searched/filtered by keyword and sources
- Created services to integrate APIs for "New York Times" and "News API"
- Created cron jobs to import articles
- Created migrations
- Created docker-compose environment
- Integrated swagger for API documentation

Congratulations! You have successfully installed the "Hafeez App" on your local machine.
